Role Name
=========


Requirements
------------


Role Variables
--------------


Dependencies
------------


Example Playbook
----------------

    - hosts: all
      connection: localhost
      roles:
         - gce_firewall

License
-------

GPLv3

Author Information
------------------
jlozada2426@gmail.com
